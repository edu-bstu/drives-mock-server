import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.MediaType;

import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.MediaType.APPLICATION_JSON_UTF_8;

public class Main {

  public static final String CONTENT_DISPOSITION = "Content-Disposition";

  public static void main(String[] args) throws IOException {
    final ClientAndServer mockServer = startClientAndServer(8080);
    final String token = "213";

    final byte[] authBody = """
      {"name": "Гордеев Артем", "role": "operator"}""".getBytes(UTF_8);

    final Header header = new Header("Authorization", "Bearer " + token);
    mockServer.when(
      new HttpRequest().withPath("/api/auth/login").withContentType(APPLICATION_JSON_UTF_8).withMethod("POST")
    ).respond(new HttpResponse().withBody(authBody).withHeader(header));

    final byte[] searchBody = """
      [
        {
          "id": "5", "driveAt": "2020-05-12T22:11:00", "vrp": "A123AA31", "color": "Красный", "model": "renault", 
          "violation": null, "postId": "F123", "listId": null 
        },{
          "id": "4", "driveAt": "2020-05-12T21:23:53", "vrp": "B426TA31", "color": "Черный", "model": "bmw", 
          "violation": null, "postId": "F123", "listId": null 
        },{
          "id": "3", "driveAt": "2020-05-12T21:17:03", "vrp": "O020AM31", "color": "Белый", "model": "lada", 
          "violation": null, "postId": "F123", "listId": null 
        },{
          "id": "2", "driveAt": "2020-05-12T20:53:34", "vrp": "A001CC31", "color": "Белый", "model": "toyota", 
          "violation": null, "postId": "F123", "listId": null 
        },{
          "id": "1", "driveAt": "2020-05-12T20:47:08", "vrp": "A007PO46", "color": "Серый", "model": "mazda", 
          "violation": null, "postId": "F123", "listId": null 
        }
      ]""".getBytes(UTF_8);

    mockServer.when(
      new HttpRequest().withPath("/api/drives").withContentType(APPLICATION_JSON_UTF_8).withMethod("GET")
    ).respond(
      new HttpResponse().withBody(searchBody).withHeaders(header, new Header("X-Total-Count", "21517"))
    );

    final byte[] reportBody = """
      id;Время проезда;ГРЗ;Цвет;Модель;Нарушение;Комплекс фиксации;Список
      5;2020-05-12T22:11:00;A123AA31;Красный;renault;;F123;
      4;2020-05-12T21:23:53;B426TA31;Черный;bmw;;F123; 
      3;2020-05-12T21:17:03;O020AM31;Белый;lada;;F123;
      2;2020-05-12T20:53:34;A001CC31;Белый;toyota;;F123;
      1;2020-05-12T20:47:08;A007PO46;Серый;mazda;;F123; 
      """.getBytes(UTF_8);

    mockServer.when(
      new HttpRequest().withPath("/api/reports").withContentType(MediaType.parse("text/csv")).withMethod("GET")
    ).respond(
      new HttpResponse()
        .withBody(reportBody)
        .withContentType(MediaType.parse("text/csv"))
        .withHeaders(header, new Header(CONTENT_DISPOSITION, "attachment;filename=report.csv"))
    );

    final byte[] photoBody = Main.class.getResourceAsStream("drive.jpg").readAllBytes();

    mockServer
      .when(new HttpRequest().withPath("/api/drives-photos/*").withMethod("GET"))
      .respond(
        new HttpResponse()
          .withBody(photoBody)
          .withContentType(MediaType.JPEG)
        .withHeader(new Header(CONTENT_DISPOSITION, "form-data; name=\"drive.jpg\"; filename=\"drive.jpg\""))
      );
  }
}
